package com.tatahealth.EMR.Scripts.ReportsView;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Pattern;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;
import com.tatahealth.pages.ReportsView.ReportsView;

public class ReportsView_upload4 {
	ReportsView report = new ReportsView();
	public static ExtentTest logger;
	public static String UHID;

	Random random = new Random();

	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Reports.reports();
		Login_Doctor.executionName = "reportsViewUpload";
		Login_Doctor.LoginTestwithDiffrentUser("Kasu");
	}

	@AfterClass(alwaysRun = true)
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		Reports.extent.flush();
	}

	public synchronized void reportsViewUploadLunch() throws Exception {
		logger = Reports.extent.createTest("EMR_Reports View/Upload Page");
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
		report.getEmrTopMenuElement(Login_Doctor.driver, logger, "Reports").click();
		report.getEmrTopMenu(Login_Doctor.driver, logger).click();
	}
	@Test(priority = 33, groups ={ "Regression", "ReportView" })
	public void alphabetsInFromDate() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter alphabets in From Date field ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);	
		String randomAlphabets = RandomStringUtils.randomAlphabetic(8);
		report.getFromDateField(Login_Doctor.driver, logger).click();
		report.getFromDateField(Login_Doctor.driver, logger).sendKeys(randomAlphabets);
		if (report.getFromDateField(Login_Doctor.driver, logger).getText()!=randomAlphabets) {
			Assert.assertTrue(true, "From date do not accept alphabets");
		}else {
			Assert.assertTrue(false, "From date accepts alphabets");
		}
	}
	
	@Test(priority = 34, groups ={ "Regression", "ReportView" })
	public void numbersInFromDate() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter numeric in From Date field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String randomNumber = RandomStringUtils.randomNumeric(8);
		report.getFromDateField(Login_Doctor.driver, logger).click();
		report.getFromDateField(Login_Doctor.driver, logger).sendKeys(randomNumber);
		if (report.getFromDateField(Login_Doctor.driver, logger).getText()!=randomNumber) {
			Assert.assertTrue(true, "From date do not accept numbers");
		}else {
			Assert.assertTrue(false, "From date accepts numbers");
		}
	}
	@Test(priority = 35, groups ={ "Regression", "ReportView" })
	public void specialCharactersInFromDate() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter special char in  From Date field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);		
		String special = "^((?=[A-Za-z@])(?![_\\\\\\\\-]).)*$";	
		report.getFromDateField(Login_Doctor.driver, logger).click();
		report.getFromDateField(Login_Doctor.driver, logger).sendKeys(special);
		if (!Pattern.matches("[^((?=[A-Za-z0-9@])(?![_\\\\\\\\-]).)*$]",
				report.getFromDateField(Login_Doctor.driver, logger).getText())) {
			Assert.assertTrue(true, "Do not allow special characters");
		} else {
			Assert.assertTrue(false, "Allow special characters");
		}
	}
	@Test(priority = 36, groups ={ "Regression", "ReportView" })
	public void searchPatientOnlyWithFromDate() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter special char in  From Date field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		report.getFromDateField(Login_Doctor.driver, logger).click();
		report.getFromDateYear(Login_Doctor.driver, logger).click();
		report.getFromDateDay(Login_Doctor.driver, logger).click();
		report.getEmrSeachPatientBtn(Login_Doctor.driver, logger).click();
		Assert.assertTrue(report.getToDatePopupMessage(Login_Doctor.driver, logger).isDisplayed());
	}
	@Test(priority = 37, groups ={ "Regression", "ReportView" })
	public void searchPatientFromDate() throws Exception {
		logger = Reports.extent.createTest("To check when user search patient with From Date which is not registered");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		report.getFromDateField(Login_Doctor.driver, logger).click();
		report.getFromDateYear(Login_Doctor.driver, logger).click();
		report.getFromDateDay(Login_Doctor.driver, logger).click();
		report.getToDateField(Login_Doctor.driver, logger).click();
		report.getToDateYear(Login_Doctor.driver, logger).click();
		report.getToDateDay(Login_Doctor.driver, logger).click();
		report.getEmrSeachPatientBtn(Login_Doctor.driver, logger).click();
		String actualText = report.searchResult(Login_Doctor.driver, logger).getText();
		Assert.assertEquals(actualText, "No Result Found");
	}
	@Test(priority = 38, groups ={ "Regression", "ReportView" })
	public void searchPatientPastFromDate() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to select past date");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		report.getFromDateField(Login_Doctor.driver, logger).click();
		report.getFromDateYear(Login_Doctor.driver, logger).click();
		report.getFromDateDay(Login_Doctor.driver, logger).click();
		report.getToDateField(Login_Doctor.driver, logger).click();
		report.getToDateYear(Login_Doctor.driver, logger).click();
		report.getToDateDay(Login_Doctor.driver, logger).click();
		report.getEmrSeachPatientBtn(Login_Doctor.driver, logger).click();
		String actualText = report.searchResult(Login_Doctor.driver, logger).getText();
		Assert.assertEquals(actualText, "No Result Found");	
	}
	@Test(priority = 39, groups ={ "Regression", "ReportView" })
	public void fieldsOnToDate() throws Exception {
		logger = Reports.extent.createTest("To check what will happen when user click on To Date field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		report.getToDateField(Login_Doctor.driver, logger).click();
		Assert.assertTrue(report.getToDateCalender(Login_Doctor.driver, logger).isDisplayed(), "To Date Calender displayed");
	}
	@Test(priority = 40, groups ={ "Regression", "ReportView" })
	public void alphabetsInToDate() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter alphabets in To Date field ");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);
		String randomAlphabets = RandomStringUtils.randomAlphabetic(8);
		report.getToDateField(Login_Doctor.driver, logger).click();
		report.getToDateField(Login_Doctor.driver, logger).sendKeys(randomAlphabets);
		if (report.getToDateField(Login_Doctor.driver, logger).getText()!=randomAlphabets) {
			Assert.assertTrue(true, "To date do not accept alphabets");
		}else {
			Assert.assertTrue(false, "To date accepts alphabets");
		}
	}
	@Test(priority = 41, groups ={ "Regression", "ReportView" })
	public void numbersInToDate() throws Exception {
		logger = Reports.extent.createTest("To check whether user can able to enter numeric in To Date field");
		reportsViewUploadLunch();
		Web_GeneralFunctions.wait(5);	
		String randomNumber = RandomStringUtils.randomNumeric(8);
		report.getToDateField(Login_Doctor.driver, logger).click();
		report.getToDateField(Login_Doctor.driver, logger).sendKeys(randomNumber);
		if (report.getToDateField(Login_Doctor.driver, logger).getText()!=randomNumber) {
			Assert.assertTrue(true, "From date do not accept numbers");
		}else {
			Assert.assertTrue(false, "From date accepts numbers");
		}
	}
}
