
package com.tatahealth.EMR.Scripts.Consultation;

import static org.testng.Assert.assertTrue;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.pages.Consultation.LabResultPage;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class LabResultTest {
	
	public static String testName = "Blood";
	public static Integer row = 0;
	public static Integer parameterValue = 0;
	public static String savedValue = "";
	public List<String> labResults = new ArrayList<String>();
	public static String reportName = "";
	public static String savedTestName = "";
	public static Integer testNo = 0;
	
	ExtentTest logger;
	@BeforeClass(alwaysRun=true,groups= {"Regression","Appointment_Doctor"})
	public static void beforeClass() throws Exception {
		ConsultationTest currentTest = new ConsultationTest();
		currentTest.loginAndStartConsultation("Lab Result");
	}
	
	@AfterClass(groups= {"Regression","Appointment_Doctor"})
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Login_Doctor.driver");
		Reports.extent.flush();
	}
	public synchronized void clickAddButton()throws Exception{
		logger = Reports.extent.createTest("EMR Click Add button");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getAddButton(Login_Doctor.driver, logger), "Click Add button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
	
	public synchronized void clickViewTrend()throws Exception{
		
		logger = Reports.extent.createTest("EMR Click View Trend Button");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getViewTrend(Login_Doctor.driver, logger), "Click view trend button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
	
	public synchronized void clickRefreshButton()throws Exception{
		
		logger = Reports.extent.createTest("EMR Click Refresh Button");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getRefresh(Login_Doctor.driver, logger), "Click refresh button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

	
	public synchronized void clickAddTrendButton()throws Exception{
		
		logger = Reports.extent.createTest("EMR Click add trend Button");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getAddTrend(Login_Doctor.driver, logger), "Click refresh button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

	@Test(groups= {"Regression","Login"},priority=838)
	public synchronized void moveToLabResultModule()throws Exception{
		
		logger = Reports.extent.createTest("EMR Move To Lab result Module");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.moveToLabResultModule(Login_Doctor.driver, logger), "Move to print module", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		System.out.println("Move To Lab result Module");
	}

	@Test(groups= {"Regression","Login"},priority=839)
	public synchronized void clickEnterLabResult()throws Exception{
		
		logger = Reports.extent.createTest("EMR click enter lab result");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.enterLabResultButton(Login_Doctor.driver, logger), "click lab result button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("EMR click enter lab result button");

	}
	
	@Test(groups= {"Regression","Login"},priority=840)
	public synchronized void saveReportWithOutReportName()throws Exception{
		
		logger = Reports.extent.createTest("EMR save report without report name");
		LabResultPage labResult = new LabResultPage();
		clickSaveReport();
		String text = Web_GeneralFunctions.getText(labResult.getJSErrorMessage(Login_Doctor.driver, logger), "get report name error message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("Text : "+text);
		if(text.equalsIgnoreCase("Please enter the report name")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		System.out.println("EMR save report without report name");
	}
	
	@Test(groups= {"Regression","Login"},priority=841)
	public synchronized void enterReportName()throws Exception{
		
		logger = Reports.extent.createTest("EMR send report name");
		LabResultPage labResult = new LabResultPage();
		reportName = RandomStringUtils.randomAlphabetic(6);
		Web_GeneralFunctions.sendkeys(labResult.enterReportName(Login_Doctor.driver, logger), reportName, "sending random text in report name field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("Enter report name");
	}
	
	@Test(groups= {"Regression","Login"},priority=842)
	public synchronized void saveReportWithOutTestName()throws Exception{
		
		logger = Reports.extent.createTest("EMR save report without test name");
		LabResultPage labResult = new LabResultPage();
		clickSaveReport();
		String text = Web_GeneralFunctions.getText(labResult.getJSErrorMessage(Login_Doctor.driver, logger), "get report name error message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("Text : "+text);
		if(text.equalsIgnoreCase("Please enter the test")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		System.out.println("save report without test name");
	}
	
	@Test(groups= {"Regression","Login"},priority=843)
	public synchronized void addTestWithOutTestName()throws Exception{
		
		logger = Reports.extent.createTest("EMR test report without entering test name in existing field");
		LabResultPage labResult = new LabResultPage();
		clickAddButton();
		String text = Web_GeneralFunctions.getText(labResult.getJSErrorMessage(Login_Doctor.driver, logger), "get report name error message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("Text : "+text);
		if(text.equalsIgnoreCase("Please enter the test")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		System.out.println("test report without entering test name in existing field-failed because defect");
	}
	
	@Test(groups= {"Regression","Login"},priority=844)
	public synchronized void enterTestName()throws Exception{
		
		logger = Reports.extent.createTest("EMR enter test name");
		LabResultPage labResult = new LabResultPage();
		WebElement element = labResult.selectTestName(testName, row, Login_Doctor.driver, logger);
		savedTestName = element.getText();
		Web_GeneralFunctions.click(element, "Select report", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		System.out.println("saved test name : "+savedTestName);
		System.out.println("enter test name and strored the test name in a variable");
	
	}
	
	@Test(groups= {"Regression","Login"},priority=845)
	public synchronized void saveReportWithOutDate()throws Exception{
		
		logger = Reports.extent.createTest("EMR save report without selecting date");
		LabResultPage labResult = new LabResultPage();
		clickSaveReport();
		String text = Web_GeneralFunctions.getText(labResult.getJSErrorMessage(Login_Doctor.driver, logger), "get report name error message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("Text : "+text);
		if(text.equalsIgnoreCase("Please enter the date")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		System.out.println("save report without selecting date");
	}
	
	@Test(groups= {"Regression","Login"},priority=846)
	public synchronized void addTesttWithOutDate()throws Exception{
		
		logger = Reports.extent.createTest("EMR add test without selecting date");
		LabResultPage labResult = new LabResultPage();
		clickAddButton();
		String text = Web_GeneralFunctions.getText(labResult.getJSErrorMessage(Login_Doctor.driver, logger), "get report name error message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("Text : "+text);
		if(text.equalsIgnoreCase("Please enter the date")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		System.out.println("add test without selecting date");
	}
	
	
	@Test(groups= {"Regression","Login"},priority=847)
	public synchronized void selectDate()throws Exception{
		
		logger = Reports.extent.createTest("EMR select date");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.selectDate(row,Login_Doctor.driver, logger), "select date from date picker", Login_Doctor.driver, logger);
		Thread.sleep(2000);
		System.out.println("select date");
	}
	
	// 88
	@Test(groups= {"Regression","Login"},priority=848)
	public synchronized void saveReportWithOutParameterValue()throws Exception{
		
		logger = Reports.extent.createTest("EMR save report without selecting date");
		LabResultPage labResult = new LabResultPage();
		System.out.println("save report without parameter value");
		//clickSaveReport();
		Web_GeneralFunctions.click(labResult.saveReport(Login_Doctor.driver, logger), "Click save report button", Login_Doctor.driver, logger);
		//Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(labResult.getSaveReportWithoutParameterErrorMessage(Login_Doctor.driver, logger), "get parameter waning message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("Text : "+text);
		/*
		 * if(text.equalsIgnoreCase("Please enter at lease one parameter and value")) {
		 * assertTrue(true); }else { assertTrue(false); }
		 */
		Thread.sleep(3000);
		System.out.println("save report without parameter value");

	}
	
	@Test(groups= {"Regression","Login"},priority=849)
	public synchronized void deleteTestParameterRow()throws Exception{
		
		logger = Reports.extent.createTest("EMR delete parameter row");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getDeleteRowButton(row, Login_Doctor.driver, logger), "click delete row button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(labResult.getDeleteRowErrorMessage(Login_Doctor.driver, logger), "Get error message while deleting row", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if(text.equalsIgnoreCase("Can not remove the parameter as each test should be have at least one parameter.")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}

		Thread.sleep(3000);
		System.out.println("delete parameter row");
	}
	
	//**
	//@Test(groups= {"Regression","Login"},priority=973)
	public synchronized void clickAddButtonWithoutParameterValue()throws Exception{
		
		logger = Reports.extent.createTest("EMR add test button");
		LabResultPage labResult = new LabResultPage();
		clickAddButton();
		String text = Web_GeneralFunctions.getText(labResult.getSaveReportWithoutParameterErrorMessage(Login_Doctor.driver, logger), "get parameter waning message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("Text : "+text);
		if(text.equalsIgnoreCase("Please enter at lease one parameter and value")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(3000);
		System.out.println("save report without parameter value --defect");

	}
	
	

	@Test(groups= {"Regression","Login"},priority=850)
	public synchronized void enterParameterValue()throws Exception{
		
		logger = Reports.extent.createTest("EMR enter parameter value");
		LabResultPage labResult = new LabResultPage();
		Random r = new Random();
		parameterValue = r.nextInt(50);
		String freetext = RandomStringUtils.randomAlphabetic(100);

		savedValue = Web_GeneralFunctions.getAttribute(labResult.getParameter(row,testNo, Login_Doctor.driver, logger), "value", "Get attribute value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		//create anther method
		Web_GeneralFunctions.sendkeys(labResult.getParameterValue(row,testNo, Login_Doctor.driver, logger),freetext, "sending value in parameter field", Login_Doctor.driver, logger);
		Thread.sleep(500);

		Web_GeneralFunctions.clear(labResult.getParameterValue(row,testNo, Login_Doctor.driver, logger), "sending value in parameter field", Login_Doctor.driver, logger);
		//Thread.sleep(500);

		Web_GeneralFunctions.sendkeys(labResult.getParameterValue(row,testNo, Login_Doctor.driver, logger), parameterValue.toString(), "sending value in parameter field", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String value = Web_GeneralFunctions.getAttribute(labResult.getReference(row,testNo, Login_Doctor.driver, logger), "value", "get reference range value", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		savedValue += " "+parameterValue.toString()+" "+value;
		reportName = reportName.substring(0, 1).toUpperCase()+reportName.substring(1);
		labResults.clear();
		labResults.add(reportName);
		labResults.add(savedValue);
		System.out.println("enter parameter value");
	}
	
	@Test(groups= {"Regression","Login"},priority=851)
	public synchronized void clickSaveReport()throws Exception{
		
		logger = Reports.extent.createTest("EMR click save report");
		LabResultPage labResult = new LabResultPage();	
		Web_GeneralFunctions.click(labResult.saveReport(Login_Doctor.driver, logger), "Click save report button", Login_Doctor.driver, logger);
		System.out.println("click save");
		Thread.sleep(1000);
		System.out.println("click save report");
	}
	
	@Test(groups= {"Regression","Login"},priority=852)
	public synchronized void pdfCheck()throws Exception{
		boolean labResultStatus = false;
		logger = Reports.extent.createTest("EMR pdf verification");
		LabResultPage labResult = new LabResultPage();
		GlobalPrintTest gp = new GlobalPrintTest();
		Thread.sleep(5000);
		String pdfContent = gp.getPDFPage();
		System.out.println("pdf content : "+pdfContent);
		System.out.println("savedValue-" +savedValue);
		System.out.println("lab result size : "+labResults.size());
		int i =0;
		reportName = reportName.substring(0, 1).toUpperCase()+reportName.substring(1);
		System.out.println();
		if(pdfContent.contains(reportName)) {
			while(i<labResults.size()) {
				savedValue = labResults.get(i);
				if(pdfContent.contains(savedValue)) {
					labResultStatus = true;
				}else {
					labResultStatus = false;
					break;
				}
				i++;
			}
		}else {
			labResultStatus = false;
		}
		
		
		if(labResultStatus == true) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Thread.sleep(2000);
	 System.out.println("PDF validation for saved values");
	}
	
	@Test(groups= {"Regression","Login"},priority=853)
	public synchronized void printReport()throws Exception{
		
		logger = Reports.extent.createTest("EMR print report in consultation page");
		LabResultPage labResult = new LabResultPage();
		moveToLabResultModule();
		Web_GeneralFunctions.click(labResult.getPrintReport(reportName, Login_Doctor.driver, logger), "Click Print report button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		pdfCheck();
		System.out.println("print report in consultation page");
	}
	
	@Test(groups= {"Regression","Login"},priority=854)
	public synchronized void getTestNameInConsultationPage()throws Exception{
		boolean labResultStatus = false;
		logger = Reports.extent.createTest("EMR test name in consultation page");
		LabResultPage labResult = new LabResultPage();
		String text = Web_GeneralFunctions.getText(labResult.getTestNameInConsultationPage(Login_Doctor.driver, logger), "Get lab test name", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("get lab test name : "+text);
		if(text.equalsIgnoreCase(savedTestName)) {
			text = labResult.getParameterFromConsultationPage(Login_Doctor.driver, logger);
			System.out.println("inside if : saved "+ savedValue);
			if(savedValue.equalsIgnoreCase(text)) {
				labResultStatus = true;
			}else {
				labResultStatus = false;
			}
		}else {
			labResultStatus = false;
		}
		System.out.println("test name in consultation page");
	}
	
	
	@Test(groups= {"Regression","Login"},priority=855)
	public synchronized void deleteNoButtonInPopUp()throws Exception{
		
		logger = Reports.extent.createTest("EMR delete no button in pop up");
		LabResultPage labResult = new LabResultPage();
		labResults.remove(0);
		System.out.println("empty lab result : "+labResults);
		moveToLabResultModule();
		clickEnterLabResult();
		enterReportName();
		testName = "lipid";
		enterTestName();
		selectDate();
		enterParameterValue();
		//labResults.add(savedValue);
		clickAddButton();
		row++;
		Web_GeneralFunctions.click(labResult.getDeleteButton(row, Login_Doctor.driver, logger), "Click Delete button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(labResult.getNoInDeletePopUp(Login_Doctor.driver, logger), "Click No button in delete pop up", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(labResult.getTestLabel(1,Login_Doctor.driver, logger), "Get test label", Login_Doctor.driver, logger);
		System.out.println("Test text : "+text);
		Thread.sleep(1000);
		if(text.equalsIgnoreCase("Test")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		System.out.println("lab results array : "+labResults);
		System.out.println("delete no button in pop up");
	}
	
	@Test(groups= {"Regression","Login"},priority=856)
	public synchronized void deleteYesButtonInPopUp()throws Exception{
		
		logger = Reports.extent.createTest("EMR delete yes button in pop up");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getDeleteButton(row, Login_Doctor.driver, logger), "Click Delete button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(labResult.getYesInDeletePopUp(Login_Doctor.driver, logger), "Click Yes button in delete pop up", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		boolean status = labResult.getTestLabelDisplayed(1,Login_Doctor.driver, logger);
		if(status == false) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		
		Thread.sleep(1000);
		System.out.println("delete yes button in pop up");

	}
	
	@Test(groups= {"Regression","Login"},priority=857)
	public synchronized void multipleLabTestInSingleReport()throws Exception{
		
		logger = Reports.extent.createTest("EMR multiple lab test in single report");
		LabResultPage labResult = new LabResultPage();
		clickAddButton();
		testName = "Blood";
		Web_GeneralFunctions.click(labResult.getTestLabel(1, Login_Doctor.driver, logger), "Get 2nd test lable", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		enterTestName();
		selectDate();
		row = 5;
		testNo = 1;
		enterParameterValue();
	   labResults.add(savedValue);
		clickSaveReport();
		System.out.println("Lab result array : "+labResults);
		pdfCheck();
	}
	
	@Test(groups= {"Regression","Login"},priority=858)
	public synchronized void clickViewTrendWithoutTestName()throws Exception{
		
		logger = Reports.extent.createTest("EMR click view trend without test name");
		LabResultPage labResult = new LabResultPage();
		moveToLabResultModule();
		clickViewTrend();
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(labResult.getJSErrorMessage(Login_Doctor.driver, logger), "get js error message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if(text.equalsIgnoreCase("Please enter the test")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		System.out.println("click view trend without test name");
	}
	
	@Test(groups= {"Regression","Login"},priority=859)
	public synchronized void clickAddTrendWithoutTestName()throws Exception{
		
		logger = Reports.extent.createTest("EMR click add trend without test name");
		LabResultPage labResult = new LabResultPage();
		clickAddTrendButton();
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(labResult.getJSErrorMessage(Login_Doctor.driver, logger), "get js error message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if(text.equalsIgnoreCase("Please enter the test")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		System.out.println("click add trend without test name");
	}
	
	@Test(groups= {"Regression","Login"},priority=860)
	public synchronized void selectTrendTest()throws Exception{
		
		logger = Reports.extent.createTest("EMR select test name");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getTestNameFromTrendField(0, savedTestName, Login_Doctor.driver, logger), "select test name from trend list", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("select test name trend test");
		
	}
	
	
	@Test(groups= {"Regression","Login"},priority=861)
	public synchronized void clickViewTrendWithoutParameterName()throws Exception{
		
		logger = Reports.extent.createTest("EMR click view trend without parameter name");
		LabResultPage labResult = new LabResultPage();
		clickViewTrend();
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(labResult.getJSErrorMessage(Login_Doctor.driver, logger), "get js error message", Login_Doctor.driver, logger);
		System.out.println("text parameter :"+text);
		Thread.sleep(1000);
		if(text.equalsIgnoreCase("Please enter the Parameters")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}
	
	@Test(groups= {"Regression","Login"},priority=862)
	public synchronized void clickAddTrendWithoutParameterName()throws Exception{
		
		logger = Reports.extent.createTest("EMR click add trend without parameter name");
		LabResultPage labResult = new LabResultPage();
		clickAddTrendButton();
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(labResult.getJSErrorMessage(Login_Doctor.driver, logger), "get js error message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		if(text.equalsIgnoreCase("Please enter the Parameters")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
	}
	
	@Test(groups= {"Regression","Login"},priority=863)
	public synchronized void selectParameterTrend()throws Exception{
		
		logger = Reports.extent.createTest("EMR select parameter name");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.selectParameterTrend(0,Login_Doctor.driver, logger), "select parameter", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		clickViewTrend();
		String text = Web_GeneralFunctions.getText(labResult.labTrends(Login_Doctor.driver, logger), "Get lab trends label", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("lab trend label : "+text+"::::");
		if(text.equalsIgnoreCase("Lab Trends")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Web_GeneralFunctions.click(labResult.labTrendsCloseButton(Login_Doctor.driver, logger), "click close of lab trends", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}

	//**
	@Test(groups= {"Regression","Login"},priority=864)
	public synchronized void clickViewOrUploadReportButton()throws Exception{
		
		logger = Reports.extent.createTest("EMR select parameter name");
		LabResultPage labResult = new LabResultPage();
		moveToLabResultModule();
		Web_GeneralFunctions.click(labResult.viewOrUploadReport(Login_Doctor.driver, logger), "Click View or upload button", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		
	}
	//**
	@Test(groups= {"Regression","Login"},priority=865)
	public synchronized void uploadReports()throws Exception{
		
		logger = Reports.extent.createTest("EMR upload reports");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getViewOrUploadReportLink(Login_Doctor.driver, logger), "Click view/upload reports", Login_Doctor.driver, logger);
		Thread.sleep(3000);
		Web_GeneralFunctions.click(labResult.getUploadReportsLabel(Login_Doctor.driver, logger), "Get upload reports pop up label", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	
	}
	
	@Test(groups= {"Regression","Login"},priority=866)
	public synchronized void clickUploadReportsWithoutSelectingFile()throws Exception{
		
		logger = Reports.extent.createTest("EMR upload reports without selecting file");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getUploadButton(Login_Doctor.driver, logger), "Click upload reports without selecting file", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		String text = Web_GeneralFunctions.getText(labResult.getMessage(Login_Doctor.driver, logger), "Get error message", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		System.out.println("text upload : "+text);
		if(text.equalsIgnoreCase("Please select file to upload")) {
			assertTrue(true);
		}else {
			assertTrue(false);
		}
		Thread.sleep(1000);
		Web_GeneralFunctions.click(labResult.getUploadReportsCloseButton(Login_Doctor.driver, logger), "Close the upload reports", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	  //problem here
		//Web_GeneralFunctions.scrollToTop(Login_Doctor.driver);
	Thread.sleep(1000);
		Web_GeneralFunctions.click(labResult.getCloseReportModal(Login_Doctor.driver, logger), "Close the report modal", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	System.out.println("upload reports without selecting file-defect");
	}

	//** below Script will only pass in local run
	
	
 
	//@Test(groups= {"Regression","Login"},priority=991)
	public synchronized void SelectingFile()throws Exception{
		
		logger = Reports.extent.createTest("EMR upload reports without selecting file");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getSelectFile(Login_Doctor.driver, logger), "Click select file", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Robot robot = new Robot();
		//Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_DOWN);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		 
		Thread.sleep(1000);	

	}
	
	//@Test(groups= {"Regression","Login"},priority=992)
		public synchronized void rmoveAfterSelectingFile()throws Exception{
		LabResultPage labResult = new LabResultPage();

		Web_GeneralFunctions.click(labResult.getChangeFile(Login_Doctor.driver, logger), "Click Remove", Login_Doctor.driver, logger);
		Thread.sleep(1000);	
			 
		System.out.println("upload reports without selecting file-defect");
		}
	//@Test(groups= {"Regression","Login"},priority=993)
	public synchronized void clickCloseReportContainer()throws Exception{
		
		logger = Reports.extent.createTest("EMR upload reports without selecting file");
		LabResultPage labResult = new LabResultPage();
		Web_GeneralFunctions.click(labResult.getCloseReportContainer(Login_Doctor.driver, logger), "Click select file", Login_Doctor.driver, logger);
		Thread.sleep(1000);		 
	}
	
	
	//@Test(groups= {"Regression","Login"},priority=990)
		public synchronized void DeleteUploadedFile()throws Exception{
			
			logger = Reports.extent.createTest("EMR upload reports without selecting file");
			LabResultPage labResult = new LabResultPage();
			Web_GeneralFunctions.click(labResult.getSelectFile(Login_Doctor.driver, logger), "Click select file", Login_Doctor.driver, logger);
			Thread.sleep(1000);

			Robot robot = new Robot();
			//Thread.sleep(1000);
			robot.keyPress(KeyEvent.VK_DOWN);
			robot.keyRelease(KeyEvent.VK_DOWN);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			String text = Web_GeneralFunctions.getText(labResult.getMessage(Login_Doctor.driver, logger), "Get message", Login_Doctor.driver, logger);
			Thread.sleep(1000);
			System.out.println("text upload : "+text);
			if(text.equalsIgnoreCase("Report uploaded successfully")) {
				assertTrue(true);
			}else {
				assertTrue(false);
			}
			Thread.sleep(1000);
			Web_GeneralFunctions.click(labResult.getUploadReportsCloseButton(Login_Doctor.driver, logger), "Close the upload reports", Login_Doctor.driver, logger);
			Thread.sleep(1000);
		  //problem here
			//Web_GeneralFunctions.scrollToTop(Login_Doctor.driver);
			Thread.sleep(1000);
			Web_GeneralFunctions.click(labResult.getCloseReportModal(Login_Doctor.driver, logger), "Close the report modal", Login_Doctor.driver, logger);
			Thread.sleep(1000);
		System.out.println("upload reports without selecting file-defect");
		}
}
