package com.tatahealth.EMR.Scripts.UserAdministration;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.API.libraries.Reports;
import com.tatahealth.DB.Scripts.OTP;
import com.tatahealth.EMR.Scripts.Login.Login_Doctor;
import com.tatahealth.EMR.Scripts.UserProfile.UserProfile;
import com.tatahealth.EMR.pages.AppointmentDashboard.CommonsPage;
import com.tatahealth.EMR.pages.Login.LoginPage;
import com.tatahealth.EMR.pages.Login.SweetAlertPage;
import com.tatahealth.EMR.pages.UserAdministration.UserAdministration;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class UserAdmin_Doc extends UserProfile {

	@BeforeClass(alwaysRun = true)
	public static void beforeClass() throws Exception {
		Login_Doctor.LoginTest();
	//	Login_Doctor.LoginTestwithDiffrentUser("SonalinB");
		System.out.println("Initialized Driver");
	}

	@AfterClass(groups = { "Regression", "Appointment_Doctor" })
	public static void afterClass() throws Exception {
		Login_Doctor.LogoutTest();
		System.out.println("Closed Driver");
	}

	ExtentTest logger;
	UserAdministration userAdmin = new UserAdministration();
	LoginPage loginPage = new LoginPage();
	SweetAlertPage swaPage = new SweetAlertPage();
	CommonsPage commonPage = new CommonsPage();
	OTP otp =new OTP();
	
	
	
	//******************************* SignUp ********************************************************//
	
	@Test(priority = 02, groups = { "Regression", "SignUp" },enabled= false)
	public synchronized void exp1() throws Exception {

		logger = Reports.extent.createTest("EMR_CS_3");
		
		// *********************************************** create user starts here *********************************************
		Thread.sleep(10000);
		System.out.println("going to click menu");

		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.GetAddNewUserLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getTitleDdl(Login_Doctor.driver, logger), "Ms",
				"select the title as miss", Login_Doctor.driver, logger);
         String name = "Autoqwerty"+Web_GeneralFunctions.generatingRandomString();
         System.out.println("user profile Name is--- "+ name);
		Web_GeneralFunctions.sendkeys(userAdmin.getNameTextBox(Login_Doctor.driver, logger), name,
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(userAdmin.getAddressTextBox(Login_Doctor.driver, logger),
				"Emr buliding, 2nd floor, near hsr", "filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getDobCalender(Login_Doctor.driver, logger), "clicked on DOB",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getDobCalender(Login_Doctor.driver, logger), "11-06-2000", "filled dob",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getroleTypeDdl(Login_Doctor.driver, logger), "Doctor",
				"select the title as miss", Login_Doctor.driver, logger);
		String mobileNumber = Web_GeneralFunctions.getRandomNumber1(100);
		System.out.println("doctor's mobileNumber is --"+mobileNumber);
		Web_GeneralFunctions.sendkeys(userAdmin.getMobileNumberTextBox(Login_Doctor.driver, logger),
				 mobileNumber, "filled doctor name", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getbloodGroupDdl(Login_Doctor.driver, logger), "A+",
				"select the title as miss", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getEmailTextBox(Login_Doctor.driver, logger), "test@tatahealth.com",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getMultiSpecialityBtn(Login_Doctor.driver, logger),
				"clicked on Spesciality", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAndrologistCbx(Login_Doctor.driver, logger),
				"clicked on AndrologistCbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAYUSHCbx(Login_Doctor.driver, logger), "clicked on AYUSHCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getMultiSpecialityBtn(Login_Doctor.driver, logger),
				"clicked on Spesciality", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getOperationRoleListMulBtn(Login_Doctor.driver, logger),
				"clicked on OperationRoleListMulBtn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSpecialityDoctorCbx(Login_Doctor.driver, logger),
				"clicked on SpecialityDoctorCbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSupportAdminCbx(Login_Doctor.driver, logger),
				"clicked on SupportAdminCbx", Login_Doctor.driver, logger);
		
		Web_GeneralFunctions.click(userAdmin.getSupportAdminCbx(Login_Doctor.driver, logger),
				"clicked on SupportAdminCbx", Login_Doctor.driver, logger);
		//getClinicAdminCbx
		Web_GeneralFunctions.click(userAdmin.getCallCenterDoctorCbx(Login_Doctor.driver, logger),
				"clicked on CallCenterDoctorCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getMedicalCertificateCbx(Login_Doctor.driver, logger),
				"clicked on MedicalCertificateCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getLabResultEntryCbx(Login_Doctor.driver, logger),
				"clicked on LabResultEntryCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getCallCentreNurseCbx(Login_Doctor.driver, logger),
				"clicked on CallCentreNurseCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getBillingDeskCbx(Login_Doctor.driver, logger),
				"clicked on BillingDeskCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getMedicalRegNoTextBox(Login_Doctor.driver, logger), "123456789",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger), "123456789",
				"filled doctor name", Login_Doctor.driver, logger);// EMR_CS_9

		Web_GeneralFunctions.click(userAdmin.getOperationLocationListMulBtn(Login_Doctor.driver, logger),
				"clicked on OperationLocationListMulBtn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAutomationCbx(Login_Doctor.driver, logger), "clicked on AutomationCbx",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getChatCbx(Login_Doctor.driver, logger), "clicked on ChatCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getVCCbx(Login_Doctor.driver, logger), "clickedname on VCCbx",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getSaveBtn(Login_Doctor.driver, logger), "clicked on  SaveBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		//Web_GeneralFunctions.waitForElement(Login_Doctor.driver, userAdmin.getSearchUserTextBox(driver, logger));
		//**************************************create user ends here*****************************************************************//
		
		//************************************************* get userID **********************************************************//

		Web_GeneralFunctions.click(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), "clicked on  SaveBtn",
				Login_Doctor.driver, logger);
		String userName = "Autodoctor"+Web_GeneralFunctions.getRandomNumber(2);
		System.out.println("userName is --"+userName);
		Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), name,
				"filled doctor name", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getSearchUserBtn(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getSortDecBtn_userId(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",Login_Doctor.driver, logger);
		Thread.sleep(1000);
	String userId=	userAdmin.getfristUserIdAfterSort(Login_Doctor.driver, logger).getText();
	System.out.println("we are doing a signup for userId "+ userId);
		// ************************************************** userID ENDS ************************************************************//
		Thread.sleep(1000);
		Web_GeneralFunctions.click(commonPage.getlogOutImg(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",
				Login_Doctor.driver, logger);
		//****************************************** Sign Up Starts ********************************************//
		Thread.sleep(1000);
		Web_GeneralFunctions.click(loginPage.getSignUpLink(Login_Doctor.driver, logger), "Clicked on  getSignUpLink",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(loginPage.getIAgreeCbx(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(loginPage.getProceedBtn(Login_Doctor.driver, logger), "Clicked on  getlogOutImg",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.sendkeys(loginPage.getSignupClinicUserIdTbx(Login_Doctor.driver, logger), userId,
				"filled   ClinicUserIdTbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(loginPage.getSignupOrgIdTbx(Login_Doctor.driver, logger), "81", "filled  ORGID",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(loginPage.getvalidateSignUpBtn(Login_Doctor.driver, logger), "Clicked on  submit",
				Login_Doctor.driver, logger);
		Thread.sleep(100);

	 
		Web_GeneralFunctions.sendkeys(loginPage.getuserLoginNameTbx(Login_Doctor.driver, logger), userName,
				"filled   ClinicUserIdTbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(loginPage.getverifyLoginNameBtn(Login_Doctor.driver, logger), "Clicked on  verifyLoginNameBtn",
				Login_Doctor.driver, logger);
		
		Thread.sleep(100);
		String otp1 = otp.getStagingOTP(mobileNumber);
		System.out.println("............"+otp1);
		Web_GeneralFunctions.sendkeys(loginPage.getSignupOtpTbx(Login_Doctor.driver, logger), otp1,
				"filled   ClinicUserIdTbx", Login_Doctor.driver, logger);
		 
		Web_GeneralFunctions.click(loginPage.getokButtonBtn(Login_Doctor.driver, logger), "Clicked on  verifyLoginNameBtn",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(loginPage.getUserPasswordTbx(Login_Doctor.driver, logger), "Test@123",
				"filled  new password", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(loginPage.getUserconfirmPasswordTbx(Login_Doctor.driver, logger), "Test@123",
				"filled  new password", Login_Doctor.driver, logger);
		Thread.sleep(100);

		Web_GeneralFunctions.click(loginPage.getUserSignUpSaveBtn(Login_Doctor.driver, logger), "Clicked on  getUserSignUpSaveBtn",
				Login_Doctor.driver, logger);
		//***********************************************************Sign up edns ****************************************************//
		// ************** search the user and enable the force password change flag ********************************************//
		Thread.sleep(1000);

		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger),
				"clicked on  SearchUserTextBox", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), "Auto_doctor",
				"filled doctor name", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getSearchUserBtn(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getEditUserBtn(Login_Doctor.driver, logger), "Clicked on getEditUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getForcePasswordChangeCbx(Login_Doctor.driver, logger), "Clicked on getForcePasswordChangeCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSaveBtn_updatePage(Login_Doctor.driver, logger), "Clicked on getSaveBtn_updatePage to enable force password",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

     	// ************************************** Ends enable the force password change flag *********************************//
		
		
		
}
	

	//************************************ Ends ****************************************************//
	
	@Test(priority = 99, groups = { "Regression", "Add_New_User" })
	public synchronized void Add_New_User1() throws Exception {

		logger = Reports.extent.createTest("Add_New_User1");
	Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
			Login_Doctor.driver, logger);
	Thread.sleep(10000);
	Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
			"Clicked on userAdministarion", Login_Doctor.driver, logger);
	Thread.sleep(1000);

	Web_GeneralFunctions.click(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger),
			"clicked on  SearchUserTextBox", Login_Doctor.driver, logger);

	Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), "Auto_doctor",
			"filled doctor name", Login_Doctor.driver, logger);

	Web_GeneralFunctions.click(userAdmin.getSearchUserBtn(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
			Login_Doctor.driver, logger);
	Thread.sleep(1000);
	Web_GeneralFunctions.click(userAdmin.getSortDecBtn_userId(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
			Login_Doctor.driver, logger);
	Thread.sleep(1000);
String userId=	userAdmin.getfristUserIdAfterSort(Login_Doctor.driver, logger).getText();
System.out.println("we are doing a signup for userId "+ userId);
	}
	


	// = new UserAdministration();
	@Test(priority = 1, groups = { "Regression", "Add_New_User" })
	public synchronized void Add_New_User2() throws Exception {

		logger = Reports.extent.createTest("Add_New_User2");
		 
		Thread.sleep(10000);
		System.out.println("going to click menu");

		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.GetAddNewUserLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getTitleDdl(Login_Doctor.driver, logger), "Ms",
				"select the title as miss", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getNameTextBox(Login_Doctor.driver, logger), "Auto_doctor",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(userAdmin.getAddressTextBox(Login_Doctor.driver, logger),
				"Emr buliding, 2nd floor, near hsr", "filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getDobCalender(Login_Doctor.driver, logger), "clicked on DOB",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getDobCalender(Login_Doctor.driver, logger), "11-06-2000", "filled dob",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getroleTypeDdl(Login_Doctor.driver, logger), "Doctor",
				"select the title as miss", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(userAdmin.getMobileNumberTextBox(Login_Doctor.driver, logger),
				Web_GeneralFunctions.getRandomNumber1(10), "filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getbloodGroupDdl(Login_Doctor.driver, logger), "A+",
				"select the title as miss", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getEmailTextBox(Login_Doctor.driver, logger), "test@tatahealth.com",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getMultiSpecialityBtn(Login_Doctor.driver, logger),
				"clicked on Spesciality", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAndrologistCbx(Login_Doctor.driver, logger),
				"clicked on AndrologistCbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAYUSHCbx(Login_Doctor.driver, logger), "clicked on AYUSHCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getMultiSpecialityBtn(Login_Doctor.driver, logger),
				"clicked on Spesciality", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getOperationRoleListMulBtn(Login_Doctor.driver, logger),
				"clicked on OperationRoleListMulBtn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSpecialityDoctorCbx(Login_Doctor.driver, logger),
				"clicked on SpecialityDoctorCbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSupportAdminCbx(Login_Doctor.driver, logger),
				"clicked on SupportAdminCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getCallCenterDoctorCbx(Login_Doctor.driver, logger),
				"clicked on CallCenterDoctorCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getMedicalCertificateCbx(Login_Doctor.driver, logger),
				"clicked on MedicalCertificateCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getLabResultEntryCbx(Login_Doctor.driver, logger),
				"clicked on LabResultEntryCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getCallCentreNurseCbx(Login_Doctor.driver, logger),
				"clicked on CallCentreNurseCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getBillingDeskCbx(Login_Doctor.driver, logger),
				"clicked on BillingDeskCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getMedicalRegNoTextBox(Login_Doctor.driver, logger), "123456789",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger), "123456789",
				"filled doctor name", Login_Doctor.driver, logger);// EMR_CS_9

		Web_GeneralFunctions.click(userAdmin.getOperationLocationListMulBtn(Login_Doctor.driver, logger),
				"clicked on OperationLocationListMulBtn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAutomationCbx(Login_Doctor.driver, logger), "clicked on AutomationCbx",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getChatCbx(Login_Doctor.driver, logger), "clicked on ChatCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getVCCbx(Login_Doctor.driver, logger), "clicked on VCCbx",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getSaveBtn(Login_Doctor.driver, logger), "clicked on  SaveBtn",
				Login_Doctor.driver, logger);

	}

	@Test(priority = 1, groups = { "Regression", "Add_New_User" })
	public synchronized void Add_New_User3() throws Exception {

		logger = Reports.extent.createTest("Add_New_User3");
		 
		Thread.sleep(10000);
		System.out.println("going to click menu");

		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.GetAddNewUserLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getTitleDdl(Login_Doctor.driver, logger), "Ms",
				"select the title as miss", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getNameTextBox(Login_Doctor.driver, logger), "Auto_doctor",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(userAdmin.getAddressTextBox(Login_Doctor.driver, logger),
				"Emr buliding, 2nd floor, near hsr", "filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getDobCalender(Login_Doctor.driver, logger), "clicked on DOB",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getDobCalender(Login_Doctor.driver, logger), "11-06-2000", "filled dob",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getroleTypeDdl(Login_Doctor.driver, logger), "Doctor",
				"select the title as miss", Login_Doctor.driver, logger);
		Web_GeneralFunctions.sendkeys(userAdmin.getMobileNumberTextBox(Login_Doctor.driver, logger),
				Web_GeneralFunctions.getRandomNumber1(10), "filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.selectElementByVisibleText(userAdmin.getbloodGroupDdl(Login_Doctor.driver, logger), "A+",
				"select the title as miss", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getEmailTextBox(Login_Doctor.driver, logger), "test@tatahealth.com",
				"filled doctor name", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getMultiSpecialityBtn(Login_Doctor.driver, logger),
				"clicked on Spesciality", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAndrologistCbx(Login_Doctor.driver, logger),
				"clicked on AndrologistCbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAYUSHCbx(Login_Doctor.driver, logger), "clicked on AYUSHCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getMultiSpecialityBtn(Login_Doctor.driver, logger),
				"clicked on Spesciality", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getOperationRoleListMulBtn(Login_Doctor.driver, logger),
				"clicked on OperationRoleListMulBtn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSpecialityDoctorCbx(Login_Doctor.driver, logger),
				"clicked on SpecialityDoctorCbx", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSupportAdminCbx(Login_Doctor.driver, logger),
				"clicked on SupportAdminCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getCallCenterDoctorCbx(Login_Doctor.driver, logger),
				"clicked on CallCenterDoctorCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getMedicalCertificateCbx(Login_Doctor.driver, logger),
				"clicked on MedicalCertificateCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getLabResultEntryCbx(Login_Doctor.driver, logger),
				"clicked on LabResultEntryCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getCallCentreNurseCbx(Login_Doctor.driver, logger),
				"clicked on CallCentreNurseCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getBillingDeskCbx(Login_Doctor.driver, logger),
				"clicked on BillingDeskCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getMedicalRegNoTextBox(Login_Doctor.driver, logger), "123456789",
				"filled doctor name", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getOperationLocationListMulBtn(Login_Doctor.driver, logger),
				"clicked on OperationLocationListMulBtn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getAutomationCbx(Login_Doctor.driver, logger), "clicked on AutomationCbx",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getChatCbx(Login_Doctor.driver, logger), "clicked on ChatCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getVCCbx(Login_Doctor.driver, logger), "clicked on VCCbx",
				Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getSaveBtn(Login_Doctor.driver, logger), "clicked on  SaveBtn",
				Login_Doctor.driver, logger);
		// EMR_CS_10 and 11

	}

	@Test(priority = 2, groups = { "Regression", "Add_New_User" })
	public synchronized void Add_New_User4() throws Exception {

		logger = Reports.extent.createTest("Add_New_User4");

		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.GetAddNewUserLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Assert.assertTrue(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).isDisplayed(),
				"Services offered' field is implemented in User Administration - Add User page ");// EMR_CS_1

		Assert.assertEquals(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).getTagName(), "input");// EMR_CS_2
																													// and
																													// 3
		Web_GeneralFunctions.sendkeys(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger), "123456789",
				"filled ClinicServicesTextBox- numeric", Login_Doctor.driver, logger);// EMR_CS_5
		userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.sendkeys(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger), "asdfgg",
				"filled ClinicServicesTextBox- chars", Login_Doctor.driver, logger); // EMR_CS_6
		userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).clear();

		Web_GeneralFunctions.sendkeys(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger), "asd1245678",
				"filled ClinicServicesTextBox- chars", Login_Doctor.driver, logger); // EMR_CS_7
		userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.sendkeys(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger),
				"asd12teurraeuydyauriiiiiiityrtyreyteyheyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyhfcshdhkdssudiehdry8eu8rewyhfre8yr8ewr8ew49yrw9e48rferf89y45678gtyyyyyyyyy66766666iiiiiiiiiiiiitg788888888888888888888",
				"filled ClinicServicesTextBox- chars", Login_Doctor.driver, logger); // EMR_CS_7

		userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).click();
		System.out.println("------------" + userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).getAttribute("value"));
		Assert.assertEquals(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).getAttribute("value").length(), 200); // EMR_CS_7

	}

	@Test(priority = 2, groups = { "Regression", "Add_New_User" })
	public synchronized void Add_New_User5() throws Exception {
		/*
		 * validating 'Service offered' field for old user
		 * :-EMR_CS_13,EMR_CS_15,EMR_CS_15,EMR_CS_16,EMR_CS_17,EMR_CS_18
		 */ logger = Reports.extent.createTest("Add_New_User5");

		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);

	//	Web_GeneralFunctions.click(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger),"clicked on  SearchUserTextBox", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), "Auto_doctor",
				"filled doctor name", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getSearchUserBtn(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(userAdmin.getEditUserBtn(Login_Doctor.driver, logger), "Clicked on getEditUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.sendkeys(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger), "asdfgg",
				"filled ClinicServicesTextBox- chars", Login_Doctor.driver, logger); // EMR_CS_6
		userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).clear();

		Web_GeneralFunctions.click(userAdmin.getSaveBtn_updatePage(Login_Doctor.driver, logger),
				"Clicked on save buuton", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), "Auto_doctor",
				"filled doctor name", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getSearchUserBtn(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getActiveUserBtn(Login_Doctor.driver, logger), "Clicked on save buuton",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		if (swaPage.getSweetAlertCheck(Login_Doctor.driver, logger)) {
			Web_GeneralFunctions.click(swaPage.getYesBtn(Login_Doctor.driver, logger, 20),
					"Clicking on Yes in sweet Alert", Login_Doctor.driver, logger);
		}

		Web_GeneralFunctions.click(userAdmin.getEditUserBtn(Login_Doctor.driver, logger), "Clicked on getEditUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.sendkeys(userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger), "asdfgg",
				"filled ClinicServicesTextBox- chars", Login_Doctor.driver, logger); // EMR_CS_6
		userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).clear();

		Web_GeneralFunctions.click(userAdmin.getSaveBtn_updatePage(Login_Doctor.driver, logger),
				"Clicked on save buuton", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), "Auto_doctor",
				"filled doctor name", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getSearchUserBtn(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getDeactivateUserBtn(Login_Doctor.driver, logger),
				"Clicked on deactivate button", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		if (swaPage.getSweetAlertCheck(Login_Doctor.driver, logger)) {
			Web_GeneralFunctions.click(swaPage.getYesBtn(Login_Doctor.driver, logger, 20),
					"Clicking on Yes in sweet Alert", Login_Doctor.driver, logger);
		}

	}

	@Test(priority = 2, groups = { "Regression", "Add_New_User" })
	public synchronized void Add_New_User6() throws Exception {
		// update
		logger = Reports.extent.createTest("Add_New_User6");

		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger),
				"clicked on  SearchUserTextBox", Login_Doctor.driver, logger);

		Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserTextBox(Login_Doctor.driver, logger), "Auto_doctor",
				"filled doctor name", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getSearchUserBtn(Login_Doctor.driver, logger), "clicked on  SearchUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(userAdmin.getEditUserBtn(Login_Doctor.driver, logger), "Clicked on getEditUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);

		Web_GeneralFunctions.click(userAdmin.getOperationRoleListMulBtn(Login_Doctor.driver, logger),
				"clicked on OperationRoleListMulBtn", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSupportAdminCbx(Login_Doctor.driver, logger),
				"clicked on SupportAdminCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getCallCenterDoctorCbx(Login_Doctor.driver, logger),
				"clicked on CallCenterDoctorCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getMedicalCertificateCbx(Login_Doctor.driver, logger),
				"clicked on MedicalCertificateCbx", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(userAdmin.getChatCbx(Login_Doctor.driver, logger), "clicked on ChatCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getVCCbx(Login_Doctor.driver, logger), "clicked on VCCbx",
				Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getSaveBtn_updatePage(Login_Doctor.driver, logger),
				"Clicked on save button", Login_Doctor.driver, logger);
		Thread.sleep(1000);
	}
	
	@Test(priority = 1, groups = { "Regression", "User_Profile" })
	public synchronized void Add_New_User7() throws Exception {
		/*
		 * EMR_CS_19, EMR_CS_20,EMR_CS_23,EMR_CS_24,EMR_CS_26,EMR_CS_27
		 */
		logger = Reports.extent.createTest("Add_New_User7");
		Thread.sleep(10000);

		Web_GeneralFunctions.click(getUserInformationIcon(Login_Doctor.driver, logger),
				"Clicked on  UserInformationIcon", Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(getuserProfileLink(Login_Doctor.driver, logger), "Clicked on userAdministarion",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Assert.assertTrue(getServicesOfferedTbx(Login_Doctor.driver, logger).isDisplayed(),
				"ServicesOfferedTbx is present");

		getServicesOfferedTbx(Login_Doctor.driver, logger).clear();
		Web_GeneralFunctions.sendkeys(getServicesOfferedTbx(Login_Doctor.driver, logger), "Auto_doctor_user_profile",
				"filled ServicesOffered name", Login_Doctor.driver, logger);

		Web_GeneralFunctions.click(getSaveBtn(Login_Doctor.driver, logger), "clicked on  Save", Login_Doctor.driver,
				logger);
		Thread.sleep(1000);
		String ActualServicesOffered = getServicesOfferedTbx(Login_Doctor.driver, logger).getAttribute("value");

		Web_GeneralFunctions.waitForElement(Login_Doctor.driver,
				getUserProfileSuccessMessgage(Login_Doctor.driver, logger));
		// Assert.assertEquals(getUserProfileSuccessMessgage(Login_Doctor.driver,
		// logger).getText(), "User details have been updated successfully", "Success
		// message is not matching. Please Check");
		String userId = getUserIdTbx(Login_Doctor.driver, logger).getAttribute("value");
		Thread.sleep(1000);

		Web_GeneralFunctions.click(getUserProfileCloseBtn(Login_Doctor.driver, logger),
				"clicked on  Close button of user profile", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getEmrMenu(Login_Doctor.driver, logger), "Clicked on  emrMenu",
				Login_Doctor.driver, logger);
		Thread.sleep(10000);
		Web_GeneralFunctions.click(userAdmin.getUserAdministrationLnk(Login_Doctor.driver, logger),
				"Clicked on userAdministarion", Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.click(userAdmin.getSearchUserIdTbx(Login_Doctor.driver, logger),
				"clicked on  SearchUserIDTextBox", Login_Doctor.driver, logger);
		System.out.println("userId  === " + userId);
		Web_GeneralFunctions.sendkeys(userAdmin.getSearchUserIdTbx(Login_Doctor.driver, logger), userId,
				"filled doctor id", Login_Doctor.driver, logger);
		Web_GeneralFunctions.click(userAdmin.getEditUserBtn(Login_Doctor.driver, logger), "Clicked on getEditUserBtn",
				Login_Doctor.driver, logger);
		Thread.sleep(1000);
		Assert.assertEquals(ActualServicesOffered,
				userAdmin.getClinicServicesTextBox(Login_Doctor.driver, logger).getAttribute("value"),
				"ActualServicesOffered not matched");

	}

}
