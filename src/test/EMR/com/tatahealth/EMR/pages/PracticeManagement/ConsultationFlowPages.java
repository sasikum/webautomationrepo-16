package com.tatahealth.EMR.pages.PracticeManagement;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class ConsultationFlowPages {

	public WebElement moveToConsultationFlow(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//a[contains(text(),'Consultation Flow')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	public WebElement saveCosnultationFlow(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@class='btn btn-default takephotobtn greenbtn']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
	//symptom
	public WebElement getSymptomModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//li[@id='connect_1']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	public WebElement getParentModule(int position,WebDriver driver,ExtentTest logger)throws Exception{
		
		/*
		 * position =1 (pre-examination)
		 * position =2 (diagnosis and prescription)
		 * position = 3 (referral and follow up)
		 * position = 4(global print)
		 * position = 5 (lab result)
		 * position = 6 (case sheet)
		 */
		String xpath = "//li[@class='connectedToParent ui-sortable-handle']";
		Web_GeneralFunctions.wait(1);
		List<WebElement> li = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
			WebElement element = li.get(position);
		return element;
		
	}
	
	//follow up module
	public WebElement getFollowUpModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//li[@id='connect_13']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	//referral module
	public WebElement getReferralModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//li[@id='connect_12']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
		
	}
	
	/*
	public WebElement getDiagnosisModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//li[@class='connectedToParent ui-sortable-handle']";
		List<WebElement> li = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		
		WebElement element = li.get(2);
		return element;
		
	}*/
	
	
	public WebElement getCaseSheetModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'Case Sheet')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	
	
	
	public WebElement getLabResultModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'Lab Results')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getGlobalPrintModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'Global Print')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getReferralAndFollowUpParentModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'Referral and Follow up')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getDiagnosisAndPrescriptionModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'Diagnosis and Prescription')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getPreExaminationModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//label[contains(text(),'Pre-examination')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getVitalModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//li[@id='connect_2']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getLabTestModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//li[@id='connect_3']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getLastParentModule(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//li[@class='connectedToParent ui-sortable-handle']";
		List<WebElement> li = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		int size = li.size();
		WebElement element = li.get(size-1);
		size = size-1;
		xpath = ".//li["+size+"]/label";
		element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		String text = Web_GeneralFunctions.getText(element, "get last element text", driver, logger);
		Thread.sleep(1000);
		System.out.println("last element text : "+text);
		return element;
		
	}
	
	public WebElement getExaminationPositionInConsultationPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']//li";
		List<WebElement> li = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		WebElement element = li.get(5);
		
		return element;
		
	}
	
public WebElement getDiagnosisPositionInConsultationPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']//li";
		List<WebElement> li = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		WebElement element = li.get(0);
		
		return element;
		
	}
	

	
	public WebElement getReferralMenuInConsultationPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']//li[@data-input='Refer']";
		return  Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
		
	}
	
	public WebElement getCaseSheetMenuInConsultationPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']//li[@data-input='Examination']";
		return  Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
	}
	

	public WebElement getLabResultsMenuInConsultationPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']//li[@data-input='LabResult']";
		return  Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
	}

	public WebElement getPreExaminationMenuInConsultationPage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//ul[@id='fixedmenu']//li[@data-input='consultvitals']";
		return  Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		
	}
	
	
	
	public WebElement getDiagnosisCheckbox(WebDriver driver,ExtentTest logger) throws InterruptedException {
		String xpath = "//input[@value='Diagnosis and Prescription' and @disabled='true']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		
	}
	
	public WebElement getGlobalPrint(WebDriver driver,ExtentTest logger) throws InterruptedException {
		String xpath = "//input[@value='Global Print' and @disabled='true']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getPreExaminationCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//label[contains(text(),'Pre-examination')]//parent::li//input";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getSymptomsCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//label[contains(text(),'Symptoms')]//parent::li//input";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getVitalsCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//label[contains(text(),'Vitals')]//parent::li//input";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getLabTestsCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//label[contains(text(),'Lab tests')]//parent::li//input";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getReferralAndFollowupCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "//label[contains(text(),'Referral and Follow up')]//parent::li//input";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getReferralCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "(//label[contains(text(),'Referral')]//parent::li//input)[2]";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getFollowupCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "(//label[contains(text(),'Follow up')]//parent::li//input)[2]";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getLabResultsCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "(//label[contains(text(),'Lab Results')]//parent::li//input)[1]";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getGlobalPrintCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "(//label[contains(text(),'Global Print')]//parent::li//input)[1]";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	public WebElement getCaseSheetCheckbox(WebDriver driver,ExtentTest logger) {
		String xpath = "(//label[contains(text(),'Case Sheet')]//parent::li//input)[1]";
		return Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
	}
	
	
	
	
}
