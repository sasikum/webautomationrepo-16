
package com.tatahealth.EMR.pages.Consultation;

import com.tatahealth.API.libraries.Reports;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class SymptomPage{
	
	
	
	public WebElement selectSymptomFromDropDown(WebDriver driver,int row,String value,ExtentTest logger) throws Exception{
		
		//Web_GeneralFunctions generalFunctions = new Web_GeneralFunctions();
		String symptomField = "//input[@id='Symptom"+ row+"']";
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath(symptomField, driver, logger);
		Web_GeneralFunctions.click(element, "Click Symptom text field", driver, logger);
		
		Web_GeneralFunctions.sendkeys(element, value, "Sending search text to symptom field", driver, logger);
		Thread.sleep(1000);
		
		/*if(!value.isEmpty()) {
			
		}else {
			//Web
		}*/
		
		String xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		
		element = Web_GeneralFunctions.selectDropDown(xpath,"", driver, logger,"Select values");
				
		return element;
	}
	
	public WebElement getDuration(WebDriver driver,int row,ExtentTest logger)throws Exception{
		
		String duration = "//input[@id='Duration"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(duration, driver, logger);
		return element;
	}
	
	public WebElement getOtherDetails(WebDriver driver,int row,ExtentTest logger) throws Exception{
		
		String otherDetail = "//input[@id='OtherDetails"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(otherDetail, driver, logger);
		return element;
		
	}
	
	
	public WebElement addSymptom(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbySelector("#addSymptom", driver, logger);
		return element;
		
	}
	
	public WebElement saveSymptom(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//ul[@id='fixedmenu']/li[2]", driver, logger);
		return element;
		
	}
	
	public WebElement getSaveMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']", driver, logger);	
		return element;
	}
	
	
	public WebElement getJSErrorPopup(WebDriver driver,ExtentTest logger) throws Exception{
		
		String xpath = "//div[@class='popover fade top in popover-danger'][contains(@style,'display: block;')]/div[2]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getDeleteButtonBeforeSaving(WebDriver driver,int row,ExtentTest logger)throws Exception{
		
		String xpath = "//a[@id='delete_row"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public WebElement getSymptomModuleElementFromRightMenu(WebDriver driver,ExtentTest logger)throws Exception{
		
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//ul[@id='fixedmenu']/li[1]", driver, logger);
		return element;
	}
	
	/*public WebElement getSymptomAttribute(WebDriver driver,int row,ExtentTest logger) throws Exception{
		
		String xpath = "//input[@id='Symptom"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}*/
	
	
	public WebElement getSymptom(WebDriver driver,int row,ExtentTest logger)throws Exception{
		
		String symptomField = "//input[@id='Symptom"+ row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(symptomField, driver, logger);
		return element;
	}
	
	
	public WebElement getOtherDetailValue(WebDriver driver, int row,ExtentTest logger) throws Exception{
		
		String xpath = "//input[@id='SyOtherDetailsmptom"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	public List<WebElement> getNoMatchingSymptom(String value,int row,WebDriver driver,ExtentTest logger) throws Exception{
		
		String xpath = "//input[@id='Symptom"+ row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		Thread.sleep(1000);
		Web_GeneralFunctions.sendkeys(element, value, "Pass free text", driver, logger);
		Thread.sleep(1000);
		
		xpath = "//ul[@class='ui-menu ui-widget ui-widget-content ui-autocomplete ui-front'][starts-with(@style,'display: block;')]/li";
		List<WebElement> list = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		
		return list;
	}
	
	public WebElement getDeleteButtonAfterSaving(WebDriver driver,int row,ExtentTest logger)throws Exception{
		
		String xpath = "//button[@id='delete_row"+row+"']";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	
	
	public WebElement getSuccessMessage(WebDriver driver,ExtentTest logger)throws Exception{
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message'][contains(text(),'Vitals details have been saved successfully')]";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);	
		return element;
	}
	
}

